
\pagebreak

# Exercises for ww45

## Exercise 1 - Tcpdump and wireshark

Do this in teams of two, so we don't have too may networks.

### Exercise instructions

1. Set up a wireless AP

    There is [an official guide](https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md) to do it on raspberry.

2. Use wireshark either on your laptop or Kali (if you have a USB wireless dongle) and connect to your network.

3. Monitor the traffic.

    Are you seeing the trafic you expect?

4. Disconnect, and use wireshark again.

    Any difference?

5. set up so you can collect the wireless trafic

    See e.g. [this guide](https://null-byte.wonderhowto.com/how-to/hack-wpa-wpa2-wi-fi-passwords-with-pixie-dust-attack-using-airgeddon-0183556/) from null-byte

### Links

None supplied


## Exercise 2 - More wifi - wifite2

Remember to attack only your own stuff, or people that gave permission

### Exercise instructions

1.  test out wifite2

    Go to [null-byte](https://null-byte.wonderhowto.com/how-to/automate-wi-fi-hacking-with-wifite2-0191739/) for a guide


I had hardware issues, so this is untested.

### Links

None supplied


## Exercise 3 - NAC in action

We use IEEE 802.1x at school on cable.

### Exercise instructions

1. Take your laptop and a cable
2. Write down what you expect to see when connecting to a socket with NAC and what you see when connecting to a socket without NAC.

2. Locate 5 different active ethernet sockets

    Pick a diverse group. Classrooms, hallways, active with non-important stuff, juniper lab

3. Connect wireshark, look at the trafic, try to connect to the internet, save pcaps

    remember to note which socket you tested.

4. Compare and evaluate

### Links

None supplied

## Exercise 4a - Setting up NAC on EX4200

SRX240 doesn't support it, EX4200 do. I have been unable to establish if vSRX does.

### Exercise instructions

1. Read the [juniper docs](https://www.juniper.net/documentation/en_US/junos/topics/topic-map/802-1x-authentication-switching-devices.html)

2. make a design

3. Build it

### Links

None supplied


## Exercise 4b - Setting up WPA2 enterprise in junipelab

We have a WLC in juniperlab along with some access point.

### Exercise instructions


Yes... hmmm...

We need to ask Per about it, if you want to play with it :-)


### Links

None supplied


## Exercise 4b - Setting up a NAC bypass device

Yes, this is super cool.

### Exercise instructions

1. Understand the basic idea

    From [defcon](https://www.youtube.com/watch?v=rurYRDlf1Bo) and a [guide](https://www.scip.ch/en/?labs.20190207)

2. make a dsign and find the equipment needed

3. build it

4. test it on a NAC'ed interface



### Links

None supplied
