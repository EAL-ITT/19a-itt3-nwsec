
\pagebreak

# Exercises for ww49

## Exercise 1 - outgoing https decrypt

The palo alto firewall can intercept trafic and inspect it

### Exercise instructions

1. Setup decryption of traffic from an internal host on USR to the internet.

    Follow the [guide](https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA10g000000ClEZCA0)

3. Test that it works.

  Check the certificate as seen by the client.

### Links

* Palo alto [administrators guide on decryption](https://docs.paloaltonetworks.com/pan-os/9-0/pan-os-admin/decryption/decryption-overview.html)


## Exercise 2 - Setup URL filtering

Set up the PA to block certain URLs

### Exercise instructions

1. Setup URL filtering that asks the user if they really want to access a given site (the "continue" category)

    See palo alto [Adminstrator's Guide](https://docs.paloaltonetworks.com/pan-os/9-0/pan-os-admin/url-filtering.html)

    We will use PAN-DB for filtering.

3. test that it works on the client, and find the connections in the logs.

### Links

* Palo alto has a [URL tester](https://urlfiltering.paloaltonetworks.com/query/)



## Exercise 3 - Block/allow based on app-id

We want to block outgoing ssh traffic not going to port 22.

### Exercise instructions

1. Locate external ssh server to use

    You may need to coordinate with other groups to get access to a external server non-external.

    Or `ssh -p 20022 www.stud.mit-ucl.dk`, which may or may not work from school network.

    You may choose to work with an HTTP/S server, if you know of one running on a non.standard port number.

1. Enable app-id to allow ssh on port 22, but not on other ports.

    Other protocol will be allowed on other ports.

    Appid howto is in the [administrator's guide](https://docs.paloaltonetworks.com/pan-os/9-0/pan-os-admin/app-id/app-id-overview.html)

3. test that it works on the client, and find the connections in the logs.

### Links

* None

## Exercise 4 - wildfire

This is cool, and I have not idea how to make it into an exercise.

Read up on it and come up with ideas.

### Links

* Wildfire has is own [adminstrator's guide](https://docs.paloaltonetworks.com/wildfire/9-0/wildfire-admin/wildfire-overview.html)
* Wildfire [datasheet](https://www.paloaltonetworks.com/apps/pan/public/downloadResource?pagePath=/content/pan/en_US/resources/datasheets/wildfire)
* Wildfire ["product sheet"](https://www.paloaltonetworks.com/products/secure-the-network/wildfire) - Please enable marketing filters before going here.
