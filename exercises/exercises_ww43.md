# Exercises for ww43

## Exercise 1 - L2 protocols

Layer 2 network protocols, ie. protocols local to the subnet.

### Exercise instructions

1. set up two VMs on the same subnet, one being Kali, and bot having internet access
2. on kali, Start wireshark
2. on kali, run `arp-scan -l`

    maybe you need to install it first

4. Go through the packages in wireshark. What does arp-scan do?

    Save screenshot and pcaps.

5. There should be two Layer 2 protocols. Which ones are they?

6. Find authoritative sources that describe what they do.


### Links

None supplied


## Exercise 2 - ICMP

Layer 3 protocols leave the network

### Exercise instructions

1. reuse the setup from exercise 1
2. start wireshark
3. on kali, ping the other host
4. Locate the traffic in wireshark. Which protocol is used?
5. Find authoritative sources that describe what you see.
6. on kali, run `traceroute 8.8.8.8`
4. Locate the traffic in wireshark. Which protocol is used?
7. describe what traceroute does and how it does it.

    What would be an authorative source?

    The utility's official site, maybe?

### Links

None

## Exercise 3 - PKI

certificates and trust using x509 certificates.

### Exercise instructions

1. Read up on PKI.

    There are a lot of relevant videos and articles. You will find them yourselves.

    Remember to save links and such, that you found helpfull.

2. Do the simple-pki from [pki-tutorial](https://pki-tutorial.readthedocs.io/en/latest/index.html)
