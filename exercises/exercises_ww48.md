
\pagebreak

# Exercises for ww48

## Exercise 1 - initial PA setup

Continue working with the network from last week.

### Exercise instructions

1. You need three interface for the PA.

    Make a design, so you can access the management interface of the PA.

1. Check the hash of the PA image to detect any copy issues

    > $ sha256sum PA-VM-ESX-9.0.4.ova
    >
    > 35ef8a142c4361acee33fc6dbf3747f1fa3bed31e0bdcdf5138c6e861ec6b0a6  PA-VM-ESX-9.0.4.ova

2. Create the vm

    Either upload the vmdk+ovf file, or use vSphere. Another option is to use ovftool, e.g.

    `ovftool --name=pa-01 PA-VM-ESX-9.0.4.ova vi://root@esxi.host`

2. When login in the fist time, you must change password.

    Use both uppercase, lowercase and numbers, otherwise it might complain (and reset the screen before you can see what it says)

    Also, ensure that you log in when it says "PA-VM" at the prompt. See the forum for [details](https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA10g000000CloQCAS).


1. Set up the initial PA config, so you have SSH access

    There is a guide on [palo altos documentation](https://docs.paloaltonetworks.com/vm-series/9-0/vm-series-deployment/set-up-the-vm-series-firewall-on-hyper-v/install-the-vm-series-firewall-on-hyper-v/perform-initial-configuration-on-the-vm-series-firewall.html)

3. Test that it works.

### Links

None supplied


## Exercise 2 - Setup PA

Set up the PA as a drop in replacement for the previous router

### Exercise instructions

1. Find your documentation for the last router, ie. interfaces, ip adresses, routing and such.


1. Do a step by step configuration of the router

2. test that it works as intended.

### Links

* [Virtual routers](https://docs.paloaltonetworks.com/pan-os/7-1/pan-os-admin/networking/virtual-routers)
* [Palo alto OSPF](https://docs.paloaltonetworks.com/pan-os/8-1/pan-os-admin/networking/ospf/configure-ospf)
* [IPs and zones](https://docs.paloaltonetworks.com/pan-os/7-1/pan-os-admin/getting-started/segment-your-network-using-interfaces-and-zones/configure-interfaces-and-zones)
* [Firewall policies](https://docs.paloaltonetworks.com/pan-os/7-1/pan-os-admin/policy/security-policy/create-a-security-policy-rule)

## Exercise 3 - Bootstrap PA

PA supports creating an iso with the configurations.

### Exercise instructions

1. Read up on it.

   Docs are on [the PA website](https://docs.paloaltonetworks.com/vm-series/9-0/vm-series-deployment/bootstrap-the-vm-series-firewall.html)

1. Save the PA config to file

2. Create an .iso file

2. Test that it works as intended.

### Links

* None
