---
title: 'ITT3 network security'
subtitle: 'Exercises'
#authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mob@ucl.dk'
left-header: \today
right-header: 'ITT3 network security, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://eal-itt.gitlab.io/19a-itt3-nwsec/19A_ITT3_nwsec_weekly_plans.pdf)
