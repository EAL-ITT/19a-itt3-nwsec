![Build Status](https://gitlab.com/EAL-ITT/19a-itt3-nwsec/badges/master/pipeline.svg)


# 19A-ITT3-nwsec

weekly plans, resources and other relevant stuff for the network security elective course in IT technology 3. semester autumn.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19a-itt3-nwsec/)
