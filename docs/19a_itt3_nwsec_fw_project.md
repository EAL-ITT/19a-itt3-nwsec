---
title: '19A ITT3 Network security'
subtitle: 'Firewall project'
#authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: Firewall project
skip-toc: true
---

# Introduction

We want to build a non-trivial firewall system.

The basic layout is shown below

![fw overview](fw_project_overview.png)

There are some distinct parts to this project:

Subnets:

* `peering`: This is the subnet where the firewall exchange OSPF info toeach other about possible routes. OSPF is also the way to get info about the default gateway.
* `USR`: Your internal user network for whatever users you have
* `DMZ`: You will have one or more serers in this area, which must be accessible from the outside.

Devices:
* `Filter` is an optional stand-alone device that does filtering. This will include dynamic updates.
* `Router/fw` is the firewall that handles zones, statefulness and so on. This must **not** do NAT'ing.

    You are free to choose which router/fw to use. vSRX, pfsense, openbsd, linux or whatever else you fancy. You must understand what the router does, and you must have remote access to it (in a secure manner)

* `internal user` is probably a limited linux GUI client, mostly intended for testing.
* `internal server` is a web server or other service which exposes stuff to the world.

Physical setup:

* We will use 1 or more blade servers. These must be installed.
* Physical cable connection must be done correctly also. Blades have two interfaces.

MON and HRN will need to do some changes in the lab to allow the OSPF part of the setup, and we will be back with which IP adresses to use and the OSPF parameters.

# Setting up

There are of course multiple steps to setting up a system like this.

1. Setup a gitlab project for config files and such

1. Make a design and document it.

    Remember to include interfaces, OS'es, IP ranges and so on

2. Set up the ESXi on the blades.

    This will include a decision on how to share them and how to cable them

When yo uare done with the above, the rest is an iterative process.


## Iteration 1

Suggested iteration 1 is something like this

0. Describe the minimal setup for a one-subnet system and put the docs in gitlab.

1. create a minimal router that is able to connect USR with the rest of the world, using OSPF

3. Make the internal network on the blades

4. set up a simple internal machine on that network

4. test that it works


## Iteration 2

Suggested iteration 2 is something like this

1. Describe the minimal system with both internal subnets

1. Make a minimal update to the route, so it can connect both subnets to the world

3. Make the internal network on the blades

4. set up a simple internal machine on the second network

4. test that it works

## More iterations

More iterations will follow the same template as above, and at the end we will have a fully functioning system.
