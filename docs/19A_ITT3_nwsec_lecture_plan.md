---
title: '19A ITT3 Network security'
subtitle: 'Lecture plan'
#authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: true
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait18, 19A
* Name of lecturer and date of filling in form: MON 2019-10-21
* Title of the course, module or project and ECTS: Network security elective project, 5 ECTS
* Required readings, literature or technical instructions and other background material: A good understanding of networking as taught on ITT1 and ITT2 as well as communication protocls from ITT3.

See weekly plan for details like detailed daily plan, links, references, exercises and so on.

| Teacher |  Week | Content |
| :---: | :---: | :--- |
|  MON | 43 | Network services and applications, Certificates |
|  MON | 44 | Introduction to the course |
|      |    | Monitoring traffic: tcpdump, mirror ports, netflow |
|  MON | 45 | Layers 1 and 2: Wireless, 802.1x and radius/certificates |
|  MON | 46 | Layer 3: Firewalls |
|  MON | 47 | Idem  |
|  MON | 48 | Application layer: Next-Gen firewalls |
|  MON | 49 | Idem |
|  MON | 50 | Recap and exams |
|  MON | 51 | Nothing |


# General info about the course, module or project

The purpose of the course is to introduce the student to security in networks, and how to imporve security using encryption and monitor the traffic.

## The student’s learning outcome

At the end of the course the student will be able to

* Describe what a PKI infrastructure is and how it is used.
* Generate and use certificates
* Describe and setup monitoring for security and troubleshooting purposes
* Explain, desing and set up wireless test systems and associated user authentication
* Explain what firewals are and what they are used for.
* Set up filtering in firewall

## Content

The course is formed around a project, and will include relevant technology from the curriculum. It is intended for cross-topic consolidation.

## Method

There will be a minimum of teacher lectures, and a lot of student presentations. Collaboration and initiative is expected.


## Equipment

A laptop or a VM running Kali linux.

## Projects with external collaborators (proportion of students who participated)

None at this time.

## Test form/assessment

The exam in this course is a 2 hour practical exam graded as pass or fail.

The students will be given security and networking related exercises to be performed in the given timeframe..

See exam catalogue for details on compulsory elements.
