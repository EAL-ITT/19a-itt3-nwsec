---
Week: 43
Content:  Network services and applications, Certificates
Material: See links in weekly plan
Initials: MON
---

# Week 43

This is the first week of the elective course, and MON wil not be present.

The topic for the week is network services and applications. You know most of thins in advance from communication protocols, so you are to do a brush up yourself.

An added topic is certificates. PKI is the way we deploy most certificates, and you are to have an understanding of it.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* A multi VM setup suitable for sniffing traffic going from an to the two hosts.

### Learning goals

The student can

* describe common layer 2 and layer 3 protocols
* describe and use PKI

## Deliverables

None

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time | Activity |
| :---: | :--- |
| 8:15 |  You work |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  |  You work |



## Hands-on time

See exercise document.

Exercises 1 and 2 are to get you started in well-known stuff. 


## Comments

* MON is not present, neither Tuesday, nor Wednesday. Any questions will have to wait either until next week, or using riot after Wednesday.
