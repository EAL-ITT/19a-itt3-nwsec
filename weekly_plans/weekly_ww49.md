---
Week: 49
Content:  Layers 4+: Next-gen Firewalls (2/2)
Material: See links in weekly plan
Initials: MON
---

\pagebreak

# Week 49

We continue working with the palo alto firewall.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

None

### Learning goals

The student can

* setup an unknown firewall system with basic configuration
* use simple next-gen firewall features

## Deliverables

None

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time  | Activity |
| :---: | :--- |
| 8:15  | MON introduces the topic of the day |
|       | Discussion on last weeks exercises, if appl.  |
| 9:00? | See the video on firewall |
|       | Do exercises   |

MON is mostly available and at school most of the day, please ask questions on riot, mail or show up in the teachers lounge.

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 10:30 | Status presentations |
|       | We decide how to meet again |

MON is mostly available and at school most of the day, please ask questions on riot, mail or show up in the teachers lounge.

Links:

* To come, if appl.

## Hands-on time

No exercises, but see [project description](https://eal-itt.gitlab.io/19a-itt3-nwsec/19a_itt3_nwsec_fw_project.pdf)

## Comments

* None
