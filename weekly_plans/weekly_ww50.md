---
Week: 50
Content:  Recap
Material: See links in weekly plan
Initials: MON
---

\pagebreak

# Week 50

This week we do a recap, and test if the ova files are importable.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* We have tested import of ova files

### Learning goals

Noen

## Deliverables

None

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time  | Activity |
| :---: | :--- |
| 10:14 | Recap and exam discussions   |
|  | Feedback on evaluation   |


### Wednesday

No class

Links:

* None

## Hands-on time

No exercises, but see [project description](https://eal-itt.gitlab.io/19a-itt3-nwsec/19a_itt3_nwsec_fw_project.pdf)

## Comments

* We have exams at the end of the week. Please get in touch if you hvae questions.
