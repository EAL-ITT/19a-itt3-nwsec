---
Week: 45
Content:  Layers 1 and 2
Material: See links in weekly plan
Initials: MON
---

\pagebreak

# Week 45

The topic for the week is network security on layesrs 1 and 2.

Firewalls and such are very good at stopping and working with layers 3 and up. Having physical acces on layers 1 and 2 gives rize to certain issues.


## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* The student has a raspberry pi acting as an access point

### Learning goals

The student can

* explain network based access control and it's benefits, specifically 802.1x
* setup a system to test NAC both on cable and wireless.

## Deliverables

None

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time  | Activity |
| :---: | :--- |
| 8:15  | MON introduces the topic of the day |
|       | Discussion on last weeks exercises, if appl.  |
| 9:00? | See the video on sniffing |
|       | Do exercises   |

MON is mostly available and at school most of the day, please ask questions on riot, mail or show up in the teachers lounge.

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  | Discussions on class |
| 10:00 | You work  |

MON is in meetings in the afternoon.

Links:

* NAC intro from [juniper](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwjRwNzIrdHlAhUMjqQKHYcCBw4QFjACegQIABAC&url=https%3A%2F%2Fwww.juniper.net%2Fdocumentation%2Fen_US%2Flearn-about%2FLA_802.1X_NAC.pdf&usg=AOvVaw3RDQt2RfFXYMdSmvpVyCk5)
* NAC intro from ["mikeguy"](https://mikeguy.co.uk/posts/2018/06/understanding-nac-802.1x-and-mab/)



## Hands-on time

See exercise document.

I wanted to make an exercise where we set up a WPA2 enterprise with a freeradius backend for user authentication, but I could not find good resources for you. If you want to play with it, let's discuss it.

## Comments

* There are many super cool project out there like [openwisp](http://openwisp.org/whatis.html)and [packetfence](https://packetfence.org/about.html#/overview)
