---
Week: 46
Content:  Layer 3: Firewalls (1/2)
Material: See links in weekly plan
Initials: MON
---

\pagebreak

# Week 46

The topic for this week and the next are firewalls.

We will build a small network to illustrate the topologies and methods.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* The student has a network with multiple subnets, firewall(s) and internal servers

### Learning goals

The student can

* explain firewall systems and how they are used to increase security
* setup and manage firewall system
* test and monitor a firewall

## Deliverables

None

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time  | Activity |
| :---: | :--- |
| 8:15  | MON introduces the topic of the day |
|       | Discussion on last weeks exercises, if appl.  |
| 9:00? | See the video on firewall |
|       | Do exercises   |

MON is mostly available and at school most of the day, please ask questions on riot, mail or show up in the teachers lounge.

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  | You work |
| 10:30 | Status presentations |
|       | We decide how to meet again |

MON is mostly available and at school most of the day, please ask questions on riot, mail or show up in the teachers lounge.

Links:

* Video on firewall topologies is on [youtube](https://www.youtube.com/watch?v=bvdbH5AUMe0)
* associated pdf is in [docs](https://eal-itt.gitlab.io/19a-itt3-nwsec/2019-11-11-Note-13-45-firewalls.pdf)

## Hands-on time

No ecercises, but see [project description](https://eal-itt.gitlab.io/19a-itt3-nwsec/19a_itt3_nwsec_fw_project.pdf)

## Comments

* None
