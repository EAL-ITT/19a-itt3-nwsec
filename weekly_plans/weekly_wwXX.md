---
Week: XX
Content:  Project statup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week XX

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* ..
* ..

### Learning goals
* ..
* ..

## Deliverables
* ..
* ..


## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time | Activity |
| :---: | :--- |
| 8:15 | ... |
| 9:00 |  You work |

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  | ... |
| 12:30 | Presentations and discussions |



## Hands-on time

### Exercise 0

...

### Exercise 1

...

### Exercise 2

...

## Comments
* ..
* ..
