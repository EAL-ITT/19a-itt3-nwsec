---
Week: 44
Content:  Monitoring traffic
Material: See links in weekly plan
Initials: MON
---

\pagebreak

# Week 44

The topic for the week is network trafic monitoring.

The simplest version is to use wireshark on a pc. This has been a lot. We will go through other options.

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

none

### Learning goals

The student can

* explain different sniffing strategies
* setup network monitoring

## Deliverables

None

## Schedule

Below is the tentative schedule, which may be changed depending on input from the students.

### Tuesday

| Time  | Activity |
| :---: | :--- |
| 8:15  | MON introduces the course |
|       | Discussion on last weeks exercises  |
| 9:00? | See the video on sniffing |
|       | Do exercises   |

MON is mostly available and at school most of the day, please ask questions on riot, mail or show up in the teachers lounge.

### Wednesday

| Time | Activity |
| :---: | :--- |
| 8:15  | Discussions on class |
| 10:00 | You work  |

MON is in meetings after 10 AM.

Links:

* Introduction to sniffing on [youtube](https://youtu.be/b_XQ7Xg1Jek)
* Slides are on [gitlab](https://eal-itt.gitlab.io/19a-itt3-nwsec/intro_to_snifing.pdf)

## Hands-on time

See exercise document.


## Comments

None
